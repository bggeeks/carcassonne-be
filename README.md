# Carcassoane webb app
Carcassonne is a tile-based board game in a medieval setting created by Klaus-Jürgen Wrede and published by Hans im Glück. 

This project implements Carcassonne as an open-source platform-independent computer game that supports up to five players at the same time with shared-screen multiplayer mode.

The official rules can be found [here](https://images.zmangames.com/filer_public/d5/20/d5208d61-8583-478b-a06d-b49fc9cd7aaa/zm7810_carcassonne_rules.pdf)

![](doc/preview.jpg)

## Features
Tilesets
- Base game
- ~~The river~~
- Farmers
- ~~Abbots~~
- ~~Inns and cathedrals~~

Splitscreen

# Documentation

## Technical specifications

## Requirements
Functional and non functional requirements could be found [here](doc/req.pdf)

## Architecture

| Part of project | Description                                               | Technologies                  |
| --------------- | --------------------------------------------------------- | ----------------------------- |
| Back end        | API based on CQRS                                         | .NET 5, ASP.Net Core          |
| Fron end        | SPA                                                       | Angular, Type Script |
| DB              | SQL database for user management and NoSQL for user trips | MySQL Database, MongoDB   |

![](doc/app-architecture.png)

## Database scheme
![](doc/db-schema.png)

## Use-Case diagram
![](doc/use-case.png)

## Class Diagram
![](doc/class-diagram.jpg)

## Concurrency patterns
In this application we are going to use EF core. As we are making carcassone game, there wont be any operations with database which may cause modification conflicts when executed concurently.

In ASP .NET core for each request new instance of controller class is created. It is also possible to process requests asynchronously and conceptually in our game it is only possible for one player to make any changes at a time.

Basically, we are going to handle multiple requests from multiple players at the same time in turn based game where only one player's actions are not ignored. For this we are going to use **The Bulking pattern**. We are going to keep track of which player's turn it is in any particular game and ignore requests to Controller which handles game actions from all other players.

This approach will also help preventing player from making unscheduled move even if he somehow manages to avoid block on the client side (e.g. unpressable button).

![](doc/concurrency-pattern.png)


# Download & Run